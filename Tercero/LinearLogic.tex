\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{cmll}
\usepackage{mathpartir}
\begin{document}

% Materia: Introducción a la computación cuántica y
% fundamentos de lenguajes de programación
%
% Trabajo Práctico 3: Introducción a la lógica lineal.
%
% Alumno: Martín Ceresa.
%
% e-mail: martinceresa@gmail.com

\input{title}
\thetitlepage


\section{Introducción}\label{introduccion}

En este resumen explicaré la \emph{lógica lineal} basandome en el
trabajo de \emph{Philip Wadler} ``A taste of linear logic'', el cual utiliza la teoría de
\emph{Jean-Yves Girard}.

La lógica lineal formaliza la noción de una lógica que
pueda llevar de manera explicita el consumo de recursos. Mientras que en
la lógica clásica o intuisionista se puede replicar teoremas, ya que no
pierden su veracidad con el tiempo, en la lógica lineal se realiza un
tratado explicito de los recursos y \emph{verdades} que se pueden tener.
Por ejemplo, en el caso que tengas una torta, solamente la podemos comer
una sola vez, pero un teorema utilizarlo todas las veces que quieras.

Si nos concentramos en la lógica intuisionista, debido al
\emph{isomorfismo} de \emph{Curry-Howard}, sabemos que cada termino
define a un programa funcional. Lo que nos permite mover la discusión a
programas funcionales, y dado que la lógica lineal expresa programas
funcionales al igual que la intuisionista, cobra relevancia dado que nos
permite modelar un uso real de recursos en los programas funcionales.

Un ejemplo claro donde difieren dichas lógicas es en el siguiente:
\[
  f : A \to B, x : A \vdash (x , f x) : A \times B
\]

Donde \emph{f} es una variable libre que identifica a una función de
\emph{A} en \emph{B}, y \emph{x} es una variable libre de tipo \emph{A}.
Vemos claramente que \emph{x} es usada dos veces en el termino, como
primer miembro del par y como argumento de la función \emph{f}. En otras
palabras dependiendo de la implementación del lenguaje puede ser en el mejor
caso copiar un puntero, o en el peor replicar el valor de \emph{x} en memoria.

En lógica lineal no toda hipótesis o suposición puede replicarse
libremente, ni ser ignorada sin consecuencias. En general las suposiciones
se utilizaran una sola vez, por lo que el razonamiento anterior ya no es
válido.

% Al igual que la lógica intuisionista, la lógica lineal se
% relaciona fuertemente con el \emph{lambda calculo}.

Podemos reescribir el ejemplo anterior dentro de la lógica lineal
de la siguiente manera,
\[
  \langle f : A \multimap B \rangle, \langle x : A \rangle
   \not\vdash  \langle x, f \rangle : A \otimes B
\]

O en el caso de la lógica, si \emph{f} es la función que dada una torta
nos da el placer al comerla, y dada una torta \emph{x}, el par
$\langle x , f x \rangle$ sería el par que tiene la torta original y el
placer resultante de haberla comido.

En este resumen primero se mostrará una forma alternativa de representar
la lógica intuisionista poniendo en evidencia el mecanismo utilizado
para la replicación de suposiciones, y luego ver la forma de
controlarlos para presentar la lógica lineal sin perder todo el poder
expresivo de la lógica intuisionista.

\section{Lógica Intuisionista}

Siguiendo a \emph{Wadler} se presentará la lógica intuisionista basada
en deducción natural, particularmente haciendo evidente el uso de reglas
dedicadas a la duplicación y a eliminación de suposiciones. Dichas
reglas se denominan \emph{Contracción} y \emph{Debilitación}
respectivamente. Al mostrar de manera explicita la replicación de
recursos, o su eliminación, es posible identificar el problema de forma
taxativa, poniendo en evidencia cuando son necesarias dichas reglas.

Presentamos la gramática de las proposiciones a utilizar en forma de
\emph{BNF}.
\[
\sigma , \tau ::= X | \sigma \to \tau |
\sigma \times \tau | \sigma + \tau
\]

Una suposición es una secuencia de cero o más proposiciones. Un
\emph{juicio} escrito de la forma $\Gamma \vdash A $, significa que de las
suposiciones $\Gamma$ se puede concluir la proposición $A$.

Una \emph{regla} consiste en cero o más juicios escritos arriba de una
linea, y un sólo juicio por debajo. Esto significa que si todos los
juicios por arriba de la linea son derivables el de abajo también lo es.

Utilizando juicios y reglas podemos entonces dar una definición completa
para la lógica intuisionista de la siguiente manera.

\[
  \begin{array}{c}
      \inferrule*[right=Id]{ }{ A \vdash A}
    \\
    \inferrule*[right=Ex]
    { \Gamma , \Delta \vdash A }
    { \Delta , \Gamma \vdash A}
    \inferrule*[right=Con]
    { \Gamma, A , A \vdash B}
    { \Gamma, A \vdash B}
    \inferrule*[right=Wk]
    { \Gamma \vdash B}
    {\Gamma, A \vdash B}
    \\
    \inferrule*[right=$\to$-i]
    {\Gamma, A \vdash B}
    {\Gamma \vdash A \to B}
    \inferrule*[right=$\to$-e]
    {\Gamma \vdash A \to B \\ \Delta \vdash A}
    {\Gamma, \Delta \vdash B}
    \\
    \inferrule*[right=$\times$-i]
    {\Gamma \vdash A \\ \Delta \vdash B}
    {\Gamma, \Delta \vdash A \times B}
    \inferrule*[right=$\times$-i]
    {\Gamma \vdash A \times B \\ \Delta, A, B \vdash C}
    {\Gamma, \Delta \vdash C}
    \\
    \inferrule*[right=$+$-$i_1$]
    {\Gamma \vdash A}
    {\Gamma \vdash A + B}
    \inferrule*[right=$+$-$i_2$]
    {\Gamma \vdash B}
    {\Gamma \vdash A + B}
    \inferrule*[right=$+$-e]
    {\Gamma \vdash A + B \\ \Delta, A \vdash C \\ \Delta, B \vdash C}
    {\Gamma, \Delta \vdash C}
    \end{array}
\]

Prestar especial atención a las reglas \emph{Intercambio} ($Ex$),
\emph{Contracción} ($Con$) y \emph{Debilitación} ($Wk$). Estas reglas
nos permiten manipular e interactuar el contexto, es decir, las
suposiciones en un juicio determinado.  Particularmente
\emph{Contracción} nos permite replicar suposiciones,
\emph{Debilitación} nos permite eliminar suposiciones y por último
\emph{Intercambio} nos permite ignorar el orden en que se
encuentran. De esta manera se tiene un control estricto y
principalmente explicito del manejo de suposiciones, aunque como ya
hemos visto, en la lógica intuisionista no es controlado.

Por qué entonces decimos que es una forma alternativa de presentar la
lógica intuisionista? Qué diferencias tiene esta forma con las otras? Es
alternativa, porque en general se ocultan estas reglas para que el
manejo de juicios sea más sencillo. Recordar que en nuestro caso queremos
estudiar el manejo de suposiciones, y por ende nos
concentraremos en que no se nos pase nada por debajo de las narices.

Por ejemplo, podemos derivar el ejemplo anterior de la siguiente forma:

\[
  \inferrule*[right=$\times$-i]{
    \inferrule*[right=Ex]
    {
      \inferrule*[right=Wk]
      {\inferrule*[right=Id]{ }{A \vdash A}}
      {
        A , A \to B \vdash A
      }
    }
      {A \to B , A \vdash A}
      \\
   \inferrule*[right=$\to$-e]
   { \inferrule*[right=Id]{ }{ A \to B \vdash A \to B}
     \\ \inferrule*[right=Id]{ }{ A \vdash A}} 
    {A \to B , A \vdash B}
  }
  {
    A \to B, A \vdash A \times B
  }
\]

En la derivación podemos observar cómo se duplican las suposiciones,
en particular la suposición $A$, dado que en un rama la suposición
$A \to B$ es eliminada.

\section{Lógica Lineal}

La \emph{lógica lineal} busca restringir el abuso de manipulación de
las suposiciones en los juicios, y como hemos visto en la sección
anterior, dichas manipulaciones vienen por parte de las reglas de
\emph{Debilitación} y \emph{Contracción}. En este caso la regla de
\emph{Intercambio} simplemente sirve para cambiar el orden de los
contextos para luego seguir con la derivación.

Como primer aproximación podríamos simplemente eliminar las reglas de
\emph{Debilitación} y \emph{Contracción}, pero eso implicaría un corte
muy grande en la lógica, restringiendo el poder expresivo de la
misma. El objetivo es mantener las proposiciones que podemos escribir,
pero haciendo evidente cuando es necesario y cuando no la duplicación
de recursos. Por lo que restringiéremos el uso de las reglas de forma
tal que cuando sea necesario usarlas sea \textbf{evidente} en la derivación.

Dichos cambios impactan profundamente en los conectores lógicos:
\begin{itemize}
\item reemplazaremos la implicación por una noción de \emph{consumo}, y lo
escribiremos como $\multimap$, y se pronuncia \emph{lollipop}
\item la conjunción es reemplazada por dos conectores diferentes.
  \begin{itemize}
      \item conjunción
        multiplicativa $A \otimes B$, representando que \emph{A} y \emph{B} no
        comparten recursos, o hay tanto recursos para ambos. Es decir, si se
        tiene la suposición $A \otimes B$, se tiene \emph{A} y \emph{B} como
        suposiciones, y si se tiene que derivar $A \otimes B$, se debe derivar
        \emph{A} con una parte de los recursos y a \emph{B} con la otra.
      \item conjunción aditiva $A \& B$, representando que \emph{A} y \emph{B}
        comparten recursos. Es decir, si se tiene la suposición $A \& B$,
        se tienen que utilizar los recursos actuales para derivar \emph{A} o
        \emph{B}, y si se tiene que derivar $A \& B$ podemos utilizar los
        mismo recursos para derivar \emph{A} y \emph{B} por separado.
  \end{itemize}
\item la disyunción es reemplazada por la \emph{disyunción aditiva}, $A \oplus B$
  que tanto \emph{A} y \emph{B} comparten recursos, aunque a diferencia
  de la conjunción aditiva cuando se tiene como suposición no se puede
  elegir quien se deriva.
\end{itemize}

La nueva gramática entonces es:
\[
\sigma, \tau ::= X | \sigma \multimap \tau | \sigma \otimes \tau | \sigma \& \tau
    | \sigma \oplus \tau | ! \tau
\]

Basados en la teoría de Girard, utilizaremos dos tipos de
suposiciones.  Una forma llamada lineal, que no permitirá el abuso de
la misma, es decir, no permite el uso de \emph{Debilitación} y
\emph{Contracción}, a la que escribiremos como $\langle A \rangle$. Y
otra, llamada intuisionista, que si lo permitirá, y la notamos como
$[A]$. Notando de forma estricta cuando es necesario el uso de
replicar (o eliminar) suposiciones y cuando no es necesario.

\[
\begin{array}{c}
  \inferrule*[right=$\langle\text{id}\rangle$]
  { }{ \langle A \rangle \vdash A}
  \inferrule*[right=${[ \text{id} ]}$ ]
  { }{ [ A ] \vdash A}
  \\

  \inferrule*[right=Ex]{\Gamma, \Delta \vdash A}
  {\Delta, \Gamma \vdash A}
  \inferrule*[right=Con]
  {\Gamma, [A], [A] \vdash B}{\Gamma, [A] \vdash B}
  \inferrule*[right=Wk]
  {\Gamma \vdash B}{\Gamma, [A] \vdash B}
  \\

  \inferrule*[right=$!$-i]
  {[\Gamma] \vdash A}{[\Gamma] \vdash !A}
  \inferrule*[right=$!$-e]
  {\Gamma \vdash !A \\ \Delta, [A] \vdash B}
  {\Gamma, \Delta \vdash B}
  \\

  \inferrule*[right=$\multimap$-i]
  {\Gamma, \langle A \rangle \vdash B}
  {\Gamma \vdash A \multimap B}
  \inferrule*[right=$\multimap$-e]
  {\Gamma \vdash A \multimap B \\ \Delta \vdash A}
  {\Gamma, \Delta \vdash B}
  \\

  \inferrule*[right=$\otimes$-i]
  {\Gamma \vdash A \\ \Delta \vdash B}
  {\Gamma, \Delta \vdash A \otimes B}
  \inferrule*[right=$\otimes$-e]
  {\Gamma \vdash A \otimes B \\ \Delta, \langle A \rangle, \langle B \rangle \vdash C}
  {\Gamma,\Delta \vdash C}
  \\

  \inferrule*[right=$\&$-i]
  {\Gamma \vdash A \\ \Gamma \vdash B}{\Gamma \vdash A \& B}
  \inferrule*[right=$\& \text{-e}_1$]
  {\Gamma \vdash A \& B}{\Gamma \vdash A}
  \inferrule*[right=$\&\text{-e}_2$]
  {\Gamma \vdash A \& B}{\Gamma \vdash B}
  \\

  \inferrule*[right=$\oplus\text{-i}_1$]
  {\Gamma \vdash A}{\Gamma \vdash A \oplus B}
  \inferrule*[right=$\oplus\text{-i}_2$]
  {\Gamma \vdash B}{\Gamma \vdash A \oplus B}
  \inferrule*[right=$\oplus$-e]
  {\Gamma \vdash A \oplus B \\ \Delta, \langle A \rangle \vdash C
  \\ \Delta, \langle B \rangle \vdash C}
  {\Gamma, \Delta \vdash C}

\end{array}
\]

Retomando el ejemplo anterior, intentemos probar que
dada una torta podemos comerla y guardarla intacta.
¿Cómo representamos dicha conclusión?¿ Que conjunción
utilizamos?. Claramente no hay recursos para los dos,
si separamos de forma tajante la torta y la acción de
comerla no vamos a llegar a buen puerto. Por lo que
hay que ver si podemos compartir recursos para llegar
al resultado, es decir, utilizar el operador $\&$.

\[
  \inferrule*[right=$\&$-i]
  {
    \langle A \multimap B \rangle , \langle A \rangle \vdash A
    \\
    \inferrule*[right=$\multimap$-e]
    { \inferrule*[right=$\langle\text{id}\rangle$]
      { }{\langle A \multimap B \rangle \vdash \langle A \multimap B \rangle }
      \\
      \inferrule*[right=$\langle\text{id}\rangle$]
      {  }{ \langle A \rangle \vdash A}
    }
    {\langle A \multimap B \rangle , \langle A \rangle \vdash B}
  }
  {
    \langle A \multimap B \rangle , \langle A \rangle \vdash A \& B
  }
\]

Podemos observar por el juicio
$\langle A \multimap B \rangle , \langle A \rangle \vdash A$
que no podremos derivar la conclusión como queríamos. Pero además
nos indica que tenemos una suposición de más, es decir,
tenemos sobra de recursos, con lo que podemos pensar que
nos equivocamos el uso de la conjunción.

Veamos que pasa si utilizamos la conjunción multiplicativa.

\[
  \inferrule*[right=Ex]
  {
    \inferrule*[right=$\otimes$-i]{
      \inferrule*[right=$\langle\text{id}\rangle$]
      { }{\langle A \rangle \vdash A}
      \\
      \langle A \multimap B \rangle \vdash B
    }
    {\langle A \rangle , \langle A \multimap B \rangle \vdash A \otimes B}
  }
  {\langle A \multimap B \rangle , \langle A \rangle \vdash A \otimes B}
\]

Pero ahora, por el juicio $\langle A \multimap B \rangle \vdash B$ observamos
que nos faltan recursos, no podemos estar satisfechos si no tenemos una
torta para comer.

Siguiendo la idea de las pruebas anteriores, deberíamos tener al menos
dos suposiciones de \emph{A}. Es decir:
\[
  \langle A \multimap B \rangle , \langle A \rangle ,  \langle A \rangle
  \vdash A \otimes B
\]

Siguiendo con la analogía de la introducción, deberíamos tener dos
tortas, una nos las comemos llegando a un estado de felicidad \emph{B},
y conservamos una para más adelante.

\subsubsection*{Recursos Ilimitados}

Al restringir el uso de los juicios pero mantener lo que llamamos
los juicios intuisionista podemos probar todo lo que la lógica
intuisionista puede, simplemente utilizando todos juicios de la forma
$[\Gamma ] \vdash B$. Es decir, asumimos que tenemos infinitos
recursos y volvemos a caer dentro del modelo intuisionista.

A su vez, dentro de la lógica lineal, manejamos una nueva forma de
suposiciones a las que llamamos suposiciones lineales, $\langle A \rangle$.
Las cuales nos indican que tenemos una suposición para usar, podemos
gastarla en lo que queramos, pero una sola vez. Mientras que las
suposiciones intuisionista, $[A]$, indica que tenemos infinitas
copias de esa suposición.

Podemos establecer una relación entre las dos clases de suposiciones, y
lo natural es pensar que si con una cantidad finita de recursos nos alcanza,
entonces con infinitos recursos también nos alcanzará. En decir,
si $\Gamma, \langle A \rangle \vdash B$ es derivable, entonces
$\Gamma , [ A ] \vdash B$ también lo es.

Dentro de nuestra representación de la lógica lineal tenemos a su vez
otro mecanismo de conectar suposiciones lineales e intuisionistas, el
operador $!$. La suposición $[A]$ es equivalente a la suposición
$\langle !A \rangle$. Probaremos la doble implicación.


\[
  \inferrule*[right=$\multimap$-e]
  {
    \inferrule*[right=$\langle \text{id} \rangle$]
    { }
    {\langle !A \rangle \vdash !A}
    \\
    \Gamma , [ A ] \vdash B
  }
  {\langle !A \rangle , \Gamma \vdash B}
\]

Probando que si $\Gamma , [ A ] \vdash B$ es probable,
$\Gamma, \langle A \rangle \vdash B$ también lo es.
Y si $\Gamma, \langle A \rangle \vdash B$ es probable,
$\Gamma , [ A ] \vdash B$ también lo es.

\[
  \inferrule*[right=$\multimap$-e]
  {
    \inferrule*[right=$\multimap$-i]
    {\Gamma , \langle !A \rangle \vdash B}
    {\Gamma \vdash !A \multimap B}
    \\
    \inferrule*[right=$!$-i]
    {
      \inferrule*[right=${[\text{id}]}$]
      { }
      { [A] \vdash A}
    }
    {[A] \vdash !A}
  }
  {\Gamma , [A] \vdash B}
\]


Como podemos observar en la regla de introducción del operador $!$,
si con infinitos recursos podemos probar $A$, utilizando esos
recursos podemos probar cuantas veces queramos $A$, es decir, $!A$.

\section{Conclusiones}\label{conclusiones}

Presentamos una versión alternativa de la \textbf{lógica
intuisionista}, donde se observa de manera \emph{directa} la forma en
la que se manipulan las suposiciones. Se logra aislar en dichas reglas
el problema de la duplicación de suposiciones, y la eliminación de
estas, para luego restringir su uso y presentar la \textbf{lógica
lineal} una lógica con el mismo poder que la lógica intuisionista, que
mantiene un uso consiente de la manipulación de las
suposiciones. Presentamos sus operadores, y vimos cómo se comportan,
particularmente $\otimes$, $\&$ y $!$.

\section{Ejercicios}

Ejercicios para el lector.

\begin{itemize}
\item Probar que las reglas de la conjunción aditiva y multiplicativas
  llevadas a la lógica intuisionista son equivalentes.
  \[\{ \inferrule*[right=$\times$-i]
    {\Gamma \vdash A \\ \Delta \vdash B}
    { \Gamma, \Delta \vdash A \times B}
    \inferrule*[right=$\times$-e]
    {\Gamma \vdash A \times B \\ \Delta, A , B \vdash C}
    {\Gamma, \Delta \vdash C}
    \}\]
  equivalentes a: \\
  \[ \{
    \inferrule*[right=$\times$-i']
    {\Gamma \vdash A \\ \Gamma \vdash B}
    {\Gamma \vdash A \times B}
    \inferrule*[right=$\times\text{-e}'_1$]
    {\Gamma \vdash A \times B}
    {\Gamma \vdash A}
    \inferrule*[right=$\times\text{-e}'_2$]
    {\Gamma \vdash A \times B}
    {\Gamma \vdash B}
  \}  \]
\item Probar que $\langle ! (A \& B) \rangle \vdash !A \otimes !B $
\item Probar que $\langle !A \otimes !B \rangle \vdash !(A \& B)$
\item ¿Qué diferencia a $\otimes$ y $\&$?
\item (*) Mostrar que ambas lógicas son equivalentes.
  \end{itemize}
\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
