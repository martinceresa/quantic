# Linear Logic

Se explicará en forma simple, y a modo de introducción
la *lógica lineal*.

## Introducción

En este resumen explicaré la lógica lineal desde el punto de vista de
*Philip Wadler* utilizando la teoría de *Jean-Yves Girard*.

La lógica lineal trata de formalizar la noción de tener una lógica
que pueda llevar de manera explicita el consumo de recursos. Mientras
que en la lógica clásica o intuisionista se puede replicar teoremas, ya
que no pierden su veracidad con el tiempo, en la lógica lineal
se realiza un tratado explicito de los recursos y *verdades* que se
pueden tener. Por ejemplo, en el caso que tengas una torta, solamente
la podemos comer una sola vez.

Si nos concentramos en la lógica intuisionista, debido al *isomorfismo*
de *Curry-Howard*, sabemos que cada termino define a un programa funcional.
Lo que nos permite mover la discusión a programas funcionales, y la lógica
lineal cobra relevancia dado que nos permite modelar un uso real de
recursos en los programas funcionales.

Un ejemplo claro donde difieren dichas lógicas es en el siguiente:

f : A -> B, x : A |- (x, f(x)) : A x B

Donde *f* es una variable libre que identifica a una función de *A* en *B*,
y *x* es una variable libre de tipo *A*. Vemos claramente que *x* es usada
dos veces en el termino, como primer miembro del par y como argumento de la
función *f*. En otras palabras dependiendo de la implementación del lenguaje
puede ser desde copiar un puntero, o replicando el valor de *x*.

En lógica lineal no toda hipótesis o suposición puede replicarse libremente,
ni ignorada sin consecuencias. En general las suposiciones se utilizaran una
sola vez, por lo que el razonamiento anterior ya no es válido. Al igual
que la lógica intuisionista, la lógica lineal
se relaciona fuertemente con el *lambda calculo*.

En relación al ejemplo anterior, la lógica lineal no permite escribir un programa
que replique libremente los valores.

<f : A -o B>, <x : A> |/- <x, f<x>> : A (x) B

O en el caso de la lógica, si *f* es la función que dada una torta
nos da el placer al comerla, y dada una torta *x*, el par *<x,f<x>>* sería
el par que tiene la torta original y el placer resultante de haberla comido.

En este resumen primero se mostrará una forma alternativa de representar
la lógica intuisionista poniendo en evidencia el mecanismo utilizado
para la replicación de suposiciones, y luego ver la forma de controlarlos
para presentar la lógica lineal sin perder todo el poder expresivo de la 
lógica intuisionista.

## Lógica Intuisionista
Siguiendo a *Wadler* se presentará la lógica intuisionista basada en
deducción natural, particularmente haciendo evidente el uso de reglas
dedicadas a la duplicación y a eliminación de suposiciones. Dichas reglas
se denominan *Contracción* y *Debilitación* respectivamente. Al
mostrar de manera explicita la replicación de recursos, o su eliminación,
es posible identificar el problema de forma taxativa, poniendo en evidencia
cuando son necesarias dichas reglas.

Presentamos la gramática de las proposiciones a utilizar en forma de *BNF*.

\sigma, \tau ::= X | \sigma -> \tau | \sigma x \tau | \sigma + \tau

Una suposición es una secuencia de cero o más proposiciones.
Un *juicio* escrito de la forma $\Gamma |- A$, significa que de las suposiciones
$\Gamma$ se puede concluir la proposición $A$.

Una *regla* consiste en cero o más juicios escritos arriba de una linea, y un
sólo juicio por debajo. Esto significa que si todos los juicios por arriba
de la linea son derivables el de abajo también lo es.

Utilizando juicios y reglas podemos entonces dar una definición completa para
la lógica intuisionista de la siguiente manera.

TODO: reglas!

Prestar atención a las reglas *Intercambio*, *Contracción* y
*Debilitación*.  Estas reglas nos permiten manipular e interactuar el
contexto, es decir, las suposiciones en un juicio
determinado. Particularmente *Contracción* nos permite replicar
suposiciones, *Debilitación* nos permite eliminar suposiciones y por
último *Intercambio* nos permite ignorar el orden en que se
encuentran. De esta manera se tiene un control estricto y
principalmente explicito del manejo de suposiciones, aunque como ya
hemos visto, en la lógica intuisionista no es controlado.

Por qué entonces decimos que es una forma alternativa de presentar la
lógica intuisionista? Qué diferencias tiene esta forma con las otras?
Es alternativa, porque en general se ocultan
estas reglas para que el manejo de juicios se más sencillo. Recordar
que en nuestro caso queremos tener un control estricto del manejo de
suposiciones, y por ende nos concentraremos en que no se nos pase nada
por debajo de las narices.

<!-- --- -->

<!-- Otra forma de escribir la conjunción es la siguiente: -->

<!-- TODO: Reglas -->

<!-- Estas reglas son totalmente equivalentes a las anteriores, y es posible -->
<!-- demostrarlo utilizando las reglas de *Debilitación* y *Contracción*. -->
<!-- Particularmente cuando restrinjamos el uso de dichas reglas, dichas -->
<!-- reglas no serán equivalentes, y particularmente definirán dos nociones -->
<!-- semánticas diferentes. -->

## Lógica Lineal

La lógica lineal busca restringir el abuso de manipulación
de las suposiciones en los juicios, y como hemos visto en la sección
anterior, dichas manipulaciones vienen por parte de las reglas
de *Debilitación* y *Contracción*. En este caso la regla de
*Intercambio* simplemente sirve para manipular el contexto
para luego seguir con la derivación.

Podemos simplemente eliminar las reglas de *Debilitación* y *Contracción*, pero
eso implicaría un corte muy grande en la lógica, restringiendo el poder
expresivo de la misma. El objetivo es mantener las proposiciones que podemos
escribir, pero haciendo evidente cuando es necesario y cuando no la
duplicación de recursos. Por lo que restringiéremos el uso de las reglas
de forma tal que cuando sea necesario usarlas sea evidente en la derivación.

Dichos cambios impactan profundamente en los conectores lógicos:
 * reemplazaremos la implicación por una noción de *consumo*,
   y lo escribiremos como *-o*, y se pronuncia *lollipop*
 * la conjunción es reemplazada por dos conectores diferentes.
   + conjunción multiplicativa *A (x) B*, representando que *A* y *B*
     no comparten recursos, o hay tanto recursos para ambos. Es decir,
     si se tiene la suposición *A (x) B*, se tiene *A* y *B* como suposiciones,
     y si se tiene que derivar *A (x) B*, se debe derivar *A* con una parte
     de los recursos y a *B* con la otra.
   + conjunción aditiva *A & B*, representando que *A* y *B* comparten recursos.
     Es decir, si se tiene la suposición *A & B*, se tienen que utilizar los
     recursos actuales para derivar *A* o *B*, y si se tiene que derivar
     *A & B* podemos utilizar los mismo recursos para derivar *A* y *B* por
     separado.
 * la disyunción es reemplazada por la *disyunción aditiva*, *A (+) B*
   que tanto *A* y *B* comparten recursos, aunque a diferencia de la
   conjunción aditiva cuando se tiene como suposición no se puede elegir
   quien se deriva.

Formalizamos estos cambios primero cambiando la gramática y luego las reglas.

\sigma, \tau ::= X | \sigma -o \tau | \sigma (x) \tau | \sigma & \tau
    | \sigma (+) \tau | ! \tau

TODO : Reglas Linear Logic

Basados en la teoría de Girard, utilizaremos dos tipos de suposiciones.
Una forma llamada lineal, que no permitirá el abuso de la misma, es decir,
no permite el uso de *Debilitación* y *Contracción*, a la que escribiremos
como *<A>*. Y otra, llamada intuisionista, que si lo permitirá, y la notamos como
*[A]*. Notando de forma estricta cuando es necesario el uso de replicar (o eliminar)
suposiciones y cuando no es necesario.

Por ejemplo, en lógica lineal

< A -o B > , < A > |/- A x B

Siguiendo la idea de las pruebas anteriores, deberíamos tener al menos
dos suposiciones de *A*. Es decir:

< A -o B > , < A >, < A > |- A x B

Siguiendo con la analogía de la introducción, deberíamos tener dos tortas,
una nos las comemos llegando a un estado de felicidad *B*, y conservamos
una para más adelante.

Notar que las en la lógica intuisionista tanto la conjunción
aditiva como la conjunción multiplicativa son formas **equivalentes** de
representar a la conjunción. En lógica lineal se hace la distinción
en el uso de los recursos (las suposiciones), por lo que estás reglas
difieren en si comparten recursos o no.

## Algunas Palabras de Programas?

## Conclusiones?

Presentamos una versión alternativa de la **lógica intuisionista**,
donde se observa de manera directa la forma en la que se manipulan
las suposiciones. Se logra aislar en dichas reglas el problema de
la duplicación de suposiciones, y la eliminación de estas, para luego
restringir su uso y presentar la **lógica lineal**.

Presentamos la **lógica lineal**, una lógica con el mismo poder que la lógica
intuisionista, que mantiene un uso consiente de la manipulación de las
suposiciones.
