module Linear where

infixr 90 _⊸_
data Ty : Set where
    ₁ : Ty
    _⊸_ : Ty → Ty → Ty
    _⊗_ : Ty → Ty → Ty
    _⊕_ : Ty → Ty → Ty
    _&_ : Ty → Ty → Ty
    ! : Ty → Ty

data TyS : Set where
    [_] : (τ : Ty) → TyS
    <_> : (σ : Ty) → TyS

infixl 4 _,_
data Con : Set where
    ε : Con
    _,_ : Con → TyS → Con
    _,,_ : Con → Con → Con

data Var : Con → TyS → Set where
    vz : ∀ {Γ σ} → Var (Γ , σ) σ
    vs : ∀ {τ Γ σ} → Var Γ σ → Var (Γ , τ) σ

data _≡-C_ : Con → Con → Set where
    eemp : ∀ {Γ} → Γ ≡-C ( ε ,, Γ)
    exᵣ : ∀{Γ Σ τ } →  ((Γ , τ) ,, Σ ) ≡-C (Γ ,, (Σ , τ))
    exₗ : ∀{Γ Σ τ } → (Γ ,, (Σ , τ )) ≡-C ((Γ , τ) ,, Σ )
    exᵤ : ∀{Γ Δ } → (Γ ,, Δ ) ≡-C (Δ ,, Γ)
    extrans : ∀ {Γ Σ Δ} → Γ ≡-C Σ → Σ ≡-C Δ → Γ ≡-C Δ
    exsym : ∀ {Γ Δ} → Γ ≡-C Δ → Δ ≡-C Γ

data ⟦_⟧ : Con → Set where
    e : ⟦ ε ⟧
    snoc : ∀{ Γ A } → ⟦ Γ ⟧ → ⟦ Γ , [ A ] ⟧
    concat : ∀{ Γ Δ} → ⟦ Γ ⟧ → ⟦ Δ ⟧ → ⟦ Γ ,, Δ  ⟧

data _⊢_ : Con → Ty → Set where
    idₗ : ∀{τ} → (ε , ( < τ > ) ) ⊢ τ
    idᵢ : ∀{τ} → (ε , ( [ τ ] ) ) ⊢ τ
    conEx : ∀{Γ Σ A} → Γ ≡-C Σ → Γ ⊢ A → Σ ⊢ A
    conTraction : ∀{ Γ A B } → (Γ , [ A ] , [ A ] ) ⊢ B → (Γ , [ A ]) ⊢ B
    weaking : ∀{Γ A B} → Γ ⊢ B → (Γ , [ A ]) ⊢ B
    !-i : ∀ {Γ A} → ⟦ Γ ⟧ → Γ ⊢ A → Γ ⊢ ( ! A )
    !-e : ∀ {Γ Δ A B} → Γ ⊢ ! A → (Δ , [ A ]) ⊢ B → (Γ ,, Δ) ⊢ B
    ⊸-i : ∀ {Γ A B} → (Γ , < A > ) ⊢ B → Γ ⊢ A ⊸ B
    ⊸-e : ∀ { Γ Δ A B} → Γ ⊢ (A ⊸ B) → Δ ⊢ A → (Γ ,, Δ) ⊢ B
    ⊗-i : ∀ {Γ Δ A B} → Γ ⊢ A → Δ ⊢ B → ( Γ ,, Δ) ⊢ (A ⊗ B)
    ⊗-e : ∀ {Γ Δ A B C} → Γ ⊢ (A ⊗ B) → (Δ , < A > , < B >) ⊢ C → (Γ ,, Δ) ⊢ C
    &-i : ∀ {Γ A B} → Γ ⊢ A → Γ ⊢ B → Γ ⊢ (A & B)
    &-e₁ : ∀ {Γ A B} → Γ ⊢ (A & B) → Γ ⊢ A
    &-e₂ : ∀ {Γ A B} → Γ ⊢ (A & B) → Γ ⊢ B
    ⊕-i₁ : ∀ {Γ A B} → Γ ⊢ A → Γ ⊢ (A ⊕ B)
    ⊕-i₂ : ∀ {Γ A B} → Γ ⊢ B → Γ ⊢ (A ⊕ B)
    ⊕-e : ∀ {Γ Δ A B C} → Γ ⊢ (A ⊕ B)
                        → (Δ , < A >) ⊢ C
                        → (Δ , < B >) ⊢ C
                        → (Γ ,, Δ) ⊢ C

duppingcake : ∀{A B} → ( ε , < A ⊸ B > , [ A ]) ⊢ (A ⊗ B)
duppingcake =  conTraction (conEx (exsym (extrans eemp
    (exₗ))) (⊗-i idᵢ (conEx (exsym (extrans eemp exₗ))
      (conEx exᵤ (⊸-e idₗ idᵢ)))))

duppingcake' : ∀ {A B} → ( ε , < A ⊸ B > , < A >) ⊢ (A ⊗ B)
duppingcake' = {!!}
